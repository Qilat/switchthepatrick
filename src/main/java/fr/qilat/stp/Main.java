package fr.qilat.stp;

import fr.qilat.stp.commands.*;
import fr.qilat.stp.inventory.host.HostMenu;
import fr.qilat.stp.inventory.host.PlayerTeamMenu;
import fr.qilat.stp.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        super.onEnable();

        Bukkit.getPluginCommand("heal").setExecutor(new HealCommand());
        Bukkit.getPluginCommand("revive").setExecutor(new ReviveCommand());
        Bukkit.getPluginCommand("save-inventory").setExecutor(new SaveInventoryCommand());
        Bukkit.getPluginCommand("say").setExecutor(new SayCommand());
        Bukkit.getPluginCommand("setup").setExecutor(new SetupCommand());
        Bukkit.getPluginCommand("vanish").setExecutor(new VanishCommand());

        Bukkit.getPluginManager().registerEvents(new BuildListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(), this);
        Bukkit.getPluginManager().registerEvents(new DeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new FishingRodListener(), this);
        Bukkit.getPluginManager().registerEvents(new OpenBaseMenuListener(), this);
        Bukkit.getPluginManager().registerEvents(new PortalListener(), this);

        Bukkit.getPluginManager().registerEvents(new HostMenu.Listener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerTeamMenu.Listener(), this);


        Bukkit.getServer().getWorld("world").setGameRuleValue("naturalRegeneration", "false");
        Bukkit.getServer().getWorld("world_nether").setGameRuleValue("naturalRegeneration", "false");
        Bukkit.getServer().getWorld("world_the_end").setGameRuleValue("naturalRegeneration", "false");
        Bukkit.getServer().getWorld("world").setDifficulty(Difficulty.HARD);
        Bukkit.getServer().getWorld("world_nether").setDifficulty(Difficulty.HARD);
        Bukkit.getServer().getWorld("world_the_end").setDifficulty(Difficulty.HARD);

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
