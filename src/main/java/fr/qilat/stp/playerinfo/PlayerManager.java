package fr.qilat.stp.playerinfo;

import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class PlayerManager {

    @Getter
    private static HashMap<UUID, PlayerInfo> playerInfos = new HashMap<>();

    public static PlayerInfo addPlayerInfo(Player player) {
        if (!playerInfos.containsKey(player.getUniqueId())) {
            PlayerInfo playerInfo = new PlayerInfo(player);
            playerInfos.put(player.getUniqueId(), playerInfo);
            return playerInfo;
        } else {
            PlayerInfo playerInfo = playerInfos.get(player.getUniqueId());
            if (playerInfo.getBukkitPlayer() == null)
                playerInfo.setBukkitPlayer(player);
            return playerInfos.get(player.getUniqueId());
        }
    }

}
