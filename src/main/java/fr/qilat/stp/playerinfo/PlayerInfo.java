package fr.qilat.stp.playerinfo;

import fr.qilat.stp.scoreboard.PlayerScoreboard;
import fr.qilat.stp.team.Team;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class PlayerInfo {

    @Setter
    @Getter
    private Player bukkitPlayer;
    @Getter
    private Team team = null;
    @Getter
    @Setter
    private boolean alive = true;
    @Getter
    @Setter
    private Location deathLoc = null;
    @Getter
    @Setter
    private HashMap<Integer, ItemStack> deathInventory;
    @Getter
    @Setter
    private boolean invisible = false;
    @Getter
    @Setter
    private PlayerScoreboard scoreboard;

    public PlayerInfo(Player bukkitPlayer) {
        this.bukkitPlayer = bukkitPlayer;
        this.scoreboard = new PlayerScoreboard(this);
    }

}
