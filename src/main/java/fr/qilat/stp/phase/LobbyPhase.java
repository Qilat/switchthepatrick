package fr.qilat.stp.phase;

import fr.qilat.stp.UtilItem;
import fr.qilat.stp.playerinfo.PlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class LobbyPhase {

    public static UtilItem hostItem = new UtilItem();
    public static UtilItem netherStar = new UtilItem();

    static {
        hostItem.setType(Material.REDSTONE_COMPARATOR);
        hostItem.setName("Configuration de la partie");
        netherStar.setType(Material.NETHER_STAR);
        netherStar.setName("Choisir mon équipe");
    }

    public static void setHostInventory(PlayerInfo playerInfo) {
        Player player = playerInfo.getBukkitPlayer();
        setPlayerInventory(playerInfo);
        player.getInventory().setItem(4, hostItem.clone());
    }

    public static void setPlayerInventory(PlayerInfo playerInfo) {
        Player player = playerInfo.getBukkitPlayer();
        player.getInventory().setItem(0, netherStar.clone());
    }


    public static void openPlayerMenu(PlayerInfo playerInfo) {
        Inventory inventory = Bukkit.createInventory(playerInfo.getBukkitPlayer(), 9 * 6);


        playerInfo.getBukkitPlayer().openInventory(inventory);
    }

}
