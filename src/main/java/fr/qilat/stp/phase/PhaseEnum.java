package fr.qilat.stp.phase;

public enum PhaseEnum {
    LOBBY,
    GAME,
    FINAL
}
