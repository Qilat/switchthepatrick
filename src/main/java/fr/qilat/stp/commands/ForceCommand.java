package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class ForceCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());
            if (Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())) {
                switch (strings[0]) {
                    case "pvp":
                        Config.PVP_ACTIVATED = true;
                        Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> player.sendMessage(ChatColor.GREEN + "Le PVP vient d'être activé."));
                        break;
                    case "border":
                        //TODO
                        break;
                    case "switch":
                        //TODO
                        break;
                    default:
                        commandSender.sendMessage(ChatColor.RED + "Argument non reconnu : " + strings[0] + ". Doit être pvp, border ou switch.");
                }
            } else {
                commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
            }
        } else {
            commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
        }

        return true;
    }
}
