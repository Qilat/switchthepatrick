package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.Utils;
import fr.qilat.stp.phase.LobbyPhase;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class SaveInventoryCommand implements CommandExecutor {

    public static HashMap<UUID, Type> editingPlayers = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());
            if (Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())) {
                if (editingPlayers.containsKey(playerInfo.getBukkitPlayer().getUniqueId())) {
                    if (editingPlayers.get(playerInfo.getBukkitPlayer().getUniqueId()).equals(Type.BEGIN)) {
                        Config.BEGIN_INVENTORY = Utils.fromInventoryToMap(playerInfo.getBukkitPlayer());
                    } else {
                        Config.DEATH_INVENTORY = Utils.fromInventoryToMap(playerInfo.getBukkitPlayer());
                    }
                    editingPlayers.remove(playerInfo.getBukkitPlayer().getUniqueId());
                    playerInfo.getBukkitPlayer().setGameMode(GameMode.SURVIVAL);
                    LobbyPhase.setHostInventory(playerInfo);

                } else {
                    commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
                }
            } else {
                commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
            }
        } else {
            commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
        }
        return true;
    }

    public enum Type {
        BEGIN,
        DEATH
    }
}
