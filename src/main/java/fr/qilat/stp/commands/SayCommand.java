package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class SayCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.isOp() && commandSender instanceof Player) {
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());
            boolean host = (Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId()));
            String msg = ChatColor.BOLD + (host ? (ChatColor.GRAY + "[HOST]") : (ChatColor.RED + "[ADMIN]")) + ((Player) commandSender).getDisplayName() + ChatColor.WHITE + " > " + String.join(" ", strings);
            Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> player.sendMessage(msg));
        } else {
            commandSender.sendMessage("Impossible d'utiliser cette commande.");
        }
        return true;
    }

}
