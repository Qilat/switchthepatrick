package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.phase.LobbyPhase;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.isOp()) {
            if (commandSender instanceof Player) {
                if (Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null)
                    Config.HOST.getBukkitPlayer().sendMessage(ChatColor.RED + "Vous n'êtes désormais plus l'host de cette partie.");

                Config.HOST = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());

                Config.HOST.getBukkitPlayer().getInventory().clear();
                LobbyPhase.setHostInventory(Config.HOST);

                commandSender.sendMessage(ChatColor.GREEN + "Vous êtes désormais l'host de cette partie.");
            } else {
                commandSender.sendMessage(ChatColor.RED + "Impossible d'effectuer cette commande via la console.");
            }
        } else {
            commandSender.sendMessage(ChatColor.RED + "Impossible d'effectuer cette commande sans être administrateur du serveur.");
        }

        return true;
    }


}
