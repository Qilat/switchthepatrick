package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class VanishCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());
            if (Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())) {
                if (playerInfo.isInvisible()) {
                    Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> player.showPlayer((Player) commandSender));
                } else {
                    Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> player.hidePlayer((Player) commandSender));
                }
                playerInfo.setInvisible(!playerInfo.isInvisible());

            } else {
                commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
            }
        } else {
            commandSender.sendMessage(ChatColor.RED + "Impossible d'utiliser cette commande.");
        }
        return true;
    }
}
