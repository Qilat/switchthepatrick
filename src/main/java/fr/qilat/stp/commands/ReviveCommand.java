package fr.qilat.stp.commands;

import fr.qilat.stp.Config;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReviveCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(((Player) commandSender).getUniqueId());
            if (Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())) {
                String targetName = strings[0];
                Player targetPlayer = Bukkit.getPlayer(targetName);
                if (targetPlayer != null) {
                    PlayerInfo targetPlayerInfo = PlayerManager.getPlayerInfos().get(targetPlayer.getUniqueId());
                    targetPlayer.teleport(targetPlayerInfo.getDeathLoc());
                    targetPlayer.setGameMode(GameMode.SURVIVAL);
                    targetPlayerInfo.getDeathInventory().forEach((key, value) -> targetPlayer.getInventory().setItem(key, value));
                    targetPlayerInfo.setAlive(true);
                } else {
                    commandSender.sendMessage(ChatColor.RED + "Joueur introuvable : " + targetName);
                }
            } else {
                commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
            }
        } else {
            commandSender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande.");
        }
        return true;
    }
}
