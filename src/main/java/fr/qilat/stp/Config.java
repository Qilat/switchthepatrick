package fr.qilat.stp;

import fr.qilat.stp.playerinfo.PlayerInfo;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Config {
    public static PlayerInfo HOST = null;

    public static int TEAM_AMOUNT = 2;
    public static int MAX_PLAYERS_PER_TEAM = 1;

    public static HashMap<Integer, ItemStack> BEGIN_INVENTORY = null;
    public static HashMap<Integer, ItemStack> DEATH_INVENTORY = null;

    public static int MAP_SIZE = 500;
    public static float BORDER_SPEED = 1f;
    public static int TIME_BEFORE_BORDER = 40;

    public static boolean PVP_ACTIVATED = false;
    public static boolean FRIENDLY_FIRE = false;
    public static int TIME_BEFORE_PVP = 20;
    public static int TIME_BETWEEN_SWITCH = 20;
    public static int TIME_BEFORE_FIRST_SWITCH = 40;
    public static int PLAYER_AMOUNT_SWITCH = 1;
    public static boolean INV_SWITCH = false;

    public static boolean WHITELIST_ENABLED = false;

    public static int XP_RATE = 1;

    public static boolean NETHER_ENABLED = true;
    public static boolean END_ENABLED = true;

    public static boolean POTION_ENABLED = true;

    public static boolean CUTCLEAN_ENABLED = false;
    public static boolean HASTEDBOYS_ENABLED = false;

    public static boolean CHAT_ENABLED = true;
    public static boolean FISHING_ROD_ENABLED = true;

}
