package fr.qilat.stp.inventory.host;

import fr.qilat.stp.Config;
import fr.qilat.stp.UtilItem;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class PVPMenu {

    private static final String INVENTORY_NAME = ChatColor.GREEN + "PVP";
    public static Inventory inventory;
    private static UtilItem returnItem = new UtilItem();

    private static UtilItem friendlyfireItem = new UtilItem();

    private static UtilItem timeBeforePVPItem = new UtilItem();
    private static UtilItem timeBeforePVPInc1Item = new UtilItem();
    private static UtilItem timeBeforePVPInc5Item = new UtilItem();
    private static UtilItem timeBeforePVPInc10Item = new UtilItem();
    private static UtilItem timeBeforePVPDec1Item = new UtilItem();
    private static UtilItem timeBeforePVPDec5Item = new UtilItem();
    private static UtilItem timeBeforePVPDec10Item = new UtilItem();

    static {
        returnItem.setType(Material.WOOD_DOOR);
        returnItem.setName("Retour");

        friendlyfireItem.setName("Friendly Fire " + (Config.FRIENDLY_FIRE ? "Activé" : "Désactivé"));
        friendlyfireItem.setType(Material.DIAMOND_SWORD);

        timeBeforePVPItem.setName(Config.TIME_BEFORE_PVP + " minute" + (Config.TIME_BEFORE_PVP) + " avant activation du pvp");
        timeBeforePVPItem.setType(Material.WATCH);
        timeBeforePVPDec1Item.setName("-1");
        timeBeforePVPDec1Item.setType(Material.WOOD_AXE);
        timeBeforePVPDec5Item.setName("-5");
        timeBeforePVPDec5Item.setType(Material.IRON_AXE);
        timeBeforePVPDec10Item.setName("-10");
        timeBeforePVPDec10Item.setType(Material.DIAMOND_AXE);
        timeBeforePVPInc1Item.setName("+1");
        timeBeforePVPInc1Item.setType(Material.WOOD_AXE);
        timeBeforePVPInc5Item.setName("+5");
        timeBeforePVPInc5Item.setType(Material.IRON_AXE);
        timeBeforePVPInc10Item.setName("+10");
        timeBeforePVPInc10Item.setType(Material.DIAMOND_AXE);
    }

    public static void updateInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, 4 * 6, INVENTORY_NAME);
            inventory.setItem(26, returnItem);

            inventory.setItem(1, timeBeforePVPDec10Item);
            inventory.setItem(2, timeBeforePVPDec5Item);
            inventory.setItem(3, timeBeforePVPDec1Item);
            inventory.setItem(4, timeBeforePVPItem);
            inventory.setItem(5, timeBeforePVPInc1Item);
            inventory.setItem(6, timeBeforePVPInc5Item);
            inventory.setItem(7, timeBeforePVPInc10Item);

            inventory.setItem(13, friendlyfireItem);

        }

        friendlyfireItem.setName("Friendly Fire " + (Config.FRIENDLY_FIRE ? ChatColor.GREEN + "Activé" : ChatColor.RED + "Désactivé"));
        timeBeforePVPItem.setName(Config.TIME_BEFORE_PVP + " minute" + (Config.TIME_BEFORE_PVP) + " avant activation du pvp");

    }

    public static void open(PlayerInfo playerInfo) {
        updateInventory();
        playerInfo.getBukkitPlayer().openInventory(inventory);
    }

    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onClick(InventoryClickEvent event) {
            if (PhaseManager.currentPhase == PhaseEnum.LOBBY) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getWhoClicked().getUniqueId());
                if (event.getClickedInventory().getName().equals(INVENTORY_NAME)
                        && Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(event.getWhoClicked().getUniqueId())) {
                    event.setCancelled(true);

                    switch (event.getSlot()) {
                        case 1:
                            Config.TIME_BEFORE_PVP = Math.max((Config.TIME_BEFORE_PVP - 10), 5);
                            updateInventory();
                            break;
                        case 2:
                            Config.TIME_BEFORE_PVP = Math.max((Config.TIME_BEFORE_PVP - 5), 5);
                            updateInventory();
                            break;
                        case 3:
                            Config.TIME_BEFORE_PVP = Math.max((Config.TIME_BEFORE_PVP - 1), 5);
                            updateInventory();
                            break;
                        case 5:
                            Config.TIME_BEFORE_PVP = Math.min((Config.TIME_BEFORE_PVP + 1), 120);
                            updateInventory();
                            break;
                        case 6:
                            Config.TIME_BEFORE_PVP = Math.min((Config.TIME_BEFORE_PVP + 5), 120);
                            updateInventory();
                            break;
                        case 7:
                            Config.TIME_BEFORE_PVP = Math.min((Config.TIME_BEFORE_PVP + 10), 120);
                            updateInventory();
                            break;

                        case 13:
                            Config.FRIENDLY_FIRE = !Config.FRIENDLY_FIRE;
                            updateInventory();
                            break;

                        case 26:
                            HostMenu.open(playerInfo);
                            break;
                    }

                }
            }
        }

    }


}
