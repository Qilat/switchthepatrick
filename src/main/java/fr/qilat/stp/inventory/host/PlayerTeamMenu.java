package fr.qilat.stp.inventory.host;

import fr.qilat.stp.Config;
import fr.qilat.stp.UtilItem;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class PlayerTeamMenu {

    private static final String INVENTORY_NAME = ChatColor.GREEN + "Joueurs / Teams";
    public static Inventory inventory;
    private static UtilItem returnItem = new UtilItem();
    private static UtilItem teamAmountItem = new UtilItem();
    private static UtilItem teamInc1Item = new UtilItem();
    private static UtilItem teamInc5Item = new UtilItem();
    private static UtilItem teamInc10Item = new UtilItem();
    private static UtilItem teamDec1Item = new UtilItem();
    private static UtilItem teamDec5Item = new UtilItem();
    private static UtilItem teamDec10Item = new UtilItem();
    private static UtilItem playerPerTeamAmountItem = new UtilItem();
    private static UtilItem playerPerTeamInc1Item = new UtilItem();
    private static UtilItem playerPerTeamInc2Item = new UtilItem();
    private static UtilItem playerPerTeamInc5Item = new UtilItem();
    private static UtilItem playerPerTeamDec1Item = new UtilItem();
    private static UtilItem playerPerTeamDec2Item = new UtilItem();
    private static UtilItem playerPerTeamDec5Item = new UtilItem();
    private static UtilItem sumItem = new UtilItem();

    static {
        returnItem.setType(Material.WOOD_DOOR);
        returnItem.setName("Retour");

        teamAmountItem.setName(Config.TEAM_AMOUNT + " équipes");
        teamAmountItem.setType(Material.BANNER);
        teamDec1Item.setName("-1");
        teamDec1Item.setType(Material.WOOD_AXE);
        teamDec5Item.setName("-5");
        teamDec5Item.setType(Material.IRON_AXE);
        teamDec10Item.setName("-10");
        teamDec10Item.setType(Material.DIAMOND_AXE);
        teamInc1Item.setName("+1");
        teamInc1Item.setType(Material.WOOD_AXE);
        teamInc5Item.setName("+5");
        teamInc5Item.setType(Material.IRON_AXE);
        teamInc10Item.setName("+10");
        teamInc10Item.setType(Material.DIAMOND_AXE);

        playerPerTeamAmountItem.setName(Config.MAX_PLAYERS_PER_TEAM + " joueur" + (Config.MAX_PLAYERS_PER_TEAM > 1 ? "s" : "") + " par équipe");
        playerPerTeamAmountItem.setType(Material.ARMOR_STAND);
        playerPerTeamInc1Item.setName("-1");
        playerPerTeamInc1Item.setType(Material.WOOD_AXE);
        playerPerTeamInc2Item.setName("-2");
        playerPerTeamInc2Item.setType(Material.IRON_AXE);
        playerPerTeamInc5Item.setName("-5");
        playerPerTeamInc5Item.setType(Material.DIAMOND_AXE);
        playerPerTeamDec1Item.setName("+1");
        playerPerTeamDec1Item.setType(Material.WOOD_AXE);
        playerPerTeamDec2Item.setName("+2");
        playerPerTeamDec2Item.setType(Material.IRON_AXE);
        playerPerTeamDec5Item.setName("+5");
        playerPerTeamDec5Item.setType(Material.DIAMOND_AXE);

        sumItem.setName("Résumé");
        sumItem.setType(Material.ENCHANTED_BOOK);
        sumItem.setLore(
                Config.TEAM_AMOUNT + " équipes",
                "Maximum " + Config.MAX_PLAYERS_PER_TEAM + " joueur" + (Config.MAX_PLAYERS_PER_TEAM > 1 ? "s" : "") + " par équipe",
                "Soit: " + Config.TEAM_AMOUNT * Config.MAX_PLAYERS_PER_TEAM + "joueurs au global"
        );

    }

    public static void updateInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, 9 * 6, INVENTORY_NAME);
            inventory.setItem(49, returnItem);
            inventory.setItem(10, teamDec10Item);
            inventory.setItem(11, teamDec5Item);
            inventory.setItem(12, teamDec1Item);
            inventory.setItem(13, teamAmountItem);
            inventory.setItem(14, teamInc1Item);
            inventory.setItem(15, teamInc5Item);
            inventory.setItem(16, teamInc10Item);

            inventory.setItem(19, playerPerTeamDec5Item);
            inventory.setItem(20, playerPerTeamDec2Item);
            inventory.setItem(21, playerPerTeamDec1Item);
            inventory.setItem(22, playerPerTeamAmountItem);
            inventory.setItem(23, playerPerTeamInc1Item);
            inventory.setItem(24, playerPerTeamInc2Item);
            inventory.setItem(25, playerPerTeamInc5Item);
        }

        teamAmountItem.setName(Config.TEAM_AMOUNT + " équipes");
        playerPerTeamAmountItem.setName(Config.MAX_PLAYERS_PER_TEAM + " joueur" + (Config.MAX_PLAYERS_PER_TEAM > 1 ? "s" : "") + " par équipe");

        sumItem.setLore(
                Config.TEAM_AMOUNT + " équipes",
                "Maximum " + Config.MAX_PLAYERS_PER_TEAM + " joueur" + (Config.MAX_PLAYERS_PER_TEAM > 1 ? "s" : "") + " par équipe",
                "Soit: " + Config.TEAM_AMOUNT * Config.MAX_PLAYERS_PER_TEAM + "joueurs au global"
        );
    }

    public static void open(PlayerInfo playerInfo) {
        updateInventory();
        playerInfo.getBukkitPlayer().openInventory(inventory);
    }

    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onClick(InventoryClickEvent event) {
            if (PhaseManager.currentPhase == PhaseEnum.LOBBY) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getWhoClicked().getUniqueId());
                if (Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())
                        && event.getClickedInventory().getName().equals(INVENTORY_NAME)) {
                    event.setCancelled(true);

                    switch (event.getSlot()) {
                        case 10:
                            Config.TEAM_AMOUNT = Math.max((Config.TEAM_AMOUNT - 10), 2);
                            updateInventory();
                            break;
                        case 11:
                            Config.TEAM_AMOUNT = Math.max((Config.TEAM_AMOUNT - 5), 2);
                            updateInventory();
                            break;
                        case 12:
                            Config.TEAM_AMOUNT = Math.max((Config.TEAM_AMOUNT - 1), 2);
                            updateInventory();
                            break;
                        case 14:
                            Config.TEAM_AMOUNT = Math.min((Config.TEAM_AMOUNT + 1), 100);
                            updateInventory();
                            break;
                        case 15:
                            Config.TEAM_AMOUNT = Math.min((Config.TEAM_AMOUNT + 5), 100);
                            updateInventory();
                            break;
                        case 16:
                            Config.TEAM_AMOUNT = Math.min((Config.TEAM_AMOUNT + 10), 100);
                            updateInventory();
                            break;


                        case 19:
                            Config.MAX_PLAYERS_PER_TEAM = Math.max((Config.MAX_PLAYERS_PER_TEAM - 5), 1);
                            updateInventory();
                            break;
                        case 20:
                            Config.MAX_PLAYERS_PER_TEAM = Math.max((Config.MAX_PLAYERS_PER_TEAM - 2), 1);
                            updateInventory();
                            break;
                        case 21:
                            Config.MAX_PLAYERS_PER_TEAM = Math.max((Config.MAX_PLAYERS_PER_TEAM - 1), 1);
                            updateInventory();
                            break;
                        case 23:
                            Config.MAX_PLAYERS_PER_TEAM = Math.min((Config.MAX_PLAYERS_PER_TEAM + 1), 10);
                            updateInventory();
                            break;
                        case 24:
                            Config.MAX_PLAYERS_PER_TEAM = Math.min((Config.MAX_PLAYERS_PER_TEAM + 2), 10);
                            updateInventory();
                            break;
                        case 25:
                            Config.MAX_PLAYERS_PER_TEAM = Math.min((Config.MAX_PLAYERS_PER_TEAM + 5), 10);
                            updateInventory();
                            break;


                        case 49:
                            HostMenu.open(playerInfo);
                            break;
                    }

                }
            }
        }

    }

}
