package fr.qilat.stp.inventory.host;

import fr.qilat.stp.Config;
import fr.qilat.stp.UtilItem;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class SwitchMenu {

    private static final String INVENTORY_NAME = ChatColor.GREEN + "Switch";

    public static Inventory inventory;

    private static UtilItem returnItem = new UtilItem();

    private static UtilItem timeBetweenSwitchItem = new UtilItem();
    private static UtilItem timeBetweenSwitchInc1Item = new UtilItem();
    private static UtilItem timeBetweenSwitchInc5Item = new UtilItem();
    private static UtilItem timeBetweenSwitchInc10Item = new UtilItem();
    private static UtilItem timeBetweenSwitchDec1Item = new UtilItem();
    private static UtilItem timeBetweenSwitchDec5Item = new UtilItem();
    private static UtilItem timeBetweenSwitchDec10Item = new UtilItem();

    private static UtilItem timeBeforeFirstSwitchItem = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchInc1Item = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchInc5Item = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchInc10Item = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchDec1Item = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchDec5Item = new UtilItem();
    private static UtilItem timeBeforeFirstSwitchDec10Item = new UtilItem();

    private static UtilItem playerAmountSwitchItem = new UtilItem();
    private static UtilItem playerAmountSwitchInc1Item = new UtilItem();
    private static UtilItem playerAmountSwitchDec1Item = new UtilItem();

    private static UtilItem inventorySwitchItem = new UtilItem();

    static {
        returnItem.setType(Material.WOOD_DOOR);
        returnItem.setName("Retour");

        timeBetweenSwitchItem.setName("Temps entre chaque switch : " + Config.TIME_BETWEEN_SWITCH + " minute " + (Config.TIME_BETWEEN_SWITCH > 1 ? "s" : ""));
        timeBetweenSwitchItem.setType(Material.WATCH);
        timeBetweenSwitchDec1Item.setName("-1");
        timeBetweenSwitchDec1Item.setType(Material.WOOD_AXE);
        timeBetweenSwitchDec5Item.setName("-5");
        timeBetweenSwitchDec5Item.setType(Material.IRON_AXE);
        timeBetweenSwitchDec10Item.setName("-10");
        timeBetweenSwitchDec10Item.setType(Material.DIAMOND_AXE);
        timeBetweenSwitchInc1Item.setName("+1");
        timeBetweenSwitchInc1Item.setType(Material.WOOD_AXE);
        timeBetweenSwitchInc5Item.setName("+5");
        timeBetweenSwitchInc5Item.setType(Material.IRON_AXE);
        timeBetweenSwitchInc10Item.setName("+10");
        timeBetweenSwitchInc10Item.setType(Material.DIAMOND_AXE);

        playerAmountSwitchItem.setName(Config.PLAYER_AMOUNT_SWITCH + " joueur" + (Config.PLAYER_AMOUNT_SWITCH > 1 ? "s" : "") + " switch par équipe");
        playerAmountSwitchItem.setType(Material.FEATHER);
        playerAmountSwitchDec1Item.setName("-1");
        playerAmountSwitchDec1Item.setType(Material.WOOD_AXE);
        playerAmountSwitchInc1Item.setName("+1");
        playerAmountSwitchInc1Item.setType(Material.WOOD_AXE);

        timeBeforeFirstSwitchItem.setName(Config.TIME_BEFORE_FIRST_SWITCH + " minutes avant le premier switch");
        timeBeforeFirstSwitchItem.setType(Material.WATCH);
        timeBeforeFirstSwitchDec1Item.setName("-1");
        timeBeforeFirstSwitchDec1Item.setType(Material.WOOD_AXE);
        timeBeforeFirstSwitchDec5Item.setName("-5");
        timeBeforeFirstSwitchDec5Item.setType(Material.IRON_AXE);
        timeBeforeFirstSwitchDec10Item.setName("-10");
        timeBeforeFirstSwitchDec10Item.setType(Material.DIAMOND_AXE);
        timeBeforeFirstSwitchInc1Item.setName("+1");
        timeBeforeFirstSwitchInc1Item.setType(Material.WOOD_AXE);
        timeBeforeFirstSwitchInc5Item.setName("+5");
        timeBeforeFirstSwitchInc5Item.setType(Material.IRON_AXE);
        timeBeforeFirstSwitchInc10Item.setName("+10");
        timeBeforeFirstSwitchInc10Item.setType(Material.DIAMOND_AXE);

        inventorySwitchItem.setName("Switch des inventaires : " + (Config.INV_SWITCH ? ChatColor.GREEN + "Activé" : ChatColor.RED + "Désactivé"));
        inventorySwitchItem.setType(Material.REDSTONE);

    }

    public static void updateInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, 9 * 6, INVENTORY_NAME);
            inventory.setItem(49, returnItem);

            inventory.setItem(10, timeBetweenSwitchDec10Item);
            inventory.setItem(11, timeBetweenSwitchDec5Item);
            inventory.setItem(12, timeBetweenSwitchDec1Item);
            inventory.setItem(13, timeBetweenSwitchItem);
            inventory.setItem(14, timeBetweenSwitchInc1Item);
            inventory.setItem(15, timeBetweenSwitchInc5Item);
            inventory.setItem(16, timeBetweenSwitchInc10Item);

            inventory.setItem(21, playerAmountSwitchDec1Item);
            inventory.setItem(22, playerAmountSwitchItem);
            inventory.setItem(23, playerAmountSwitchInc1Item);

            inventory.setItem(28, timeBeforeFirstSwitchDec10Item);
            inventory.setItem(29, timeBeforeFirstSwitchDec5Item);
            inventory.setItem(30, timeBeforeFirstSwitchDec1Item);
            inventory.setItem(31, timeBeforeFirstSwitchItem);
            inventory.setItem(32, timeBeforeFirstSwitchInc1Item);
            inventory.setItem(33, timeBeforeFirstSwitchInc5Item);
            inventory.setItem(34, timeBeforeFirstSwitchInc10Item);

            inventory.setItem(40, inventorySwitchItem);
        }

        timeBetweenSwitchItem.setName("Temps entre chaque switch : " + Config.TIME_BETWEEN_SWITCH + " minute " + (Config.TIME_BETWEEN_SWITCH > 1 ? "s" : ""));
        playerAmountSwitchItem.setName(Config.PLAYER_AMOUNT_SWITCH + " joueur" + (Config.PLAYER_AMOUNT_SWITCH > 1 ? "s" : "") + " switch par équipe");
        timeBeforeFirstSwitchItem.setName(Config.TIME_BEFORE_FIRST_SWITCH + " minutes avant le premier switch");
        inventorySwitchItem.setName("Switch des inventaires : " + (Config.INV_SWITCH ? ChatColor.GREEN + "Activé" : ChatColor.RED + "Désactivé"));

    }

    public static void open(PlayerInfo playerInfo) {
        updateInventory();
        playerInfo.getBukkitPlayer().openInventory(inventory);
    }

    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onClick(InventoryClickEvent event) {
            if (PhaseManager.currentPhase == PhaseEnum.LOBBY) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getWhoClicked().getUniqueId());
                if (event.getClickedInventory().getName().equals(INVENTORY_NAME)
                        && Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(event.getWhoClicked().getUniqueId())) {
                    event.setCancelled(true);

                    switch (event.getSlot()) {
                        case 10:
                            Config.TIME_BETWEEN_SWITCH = Math.max((Config.TIME_BETWEEN_SWITCH - 10), 1);
                            updateInventory();
                            break;
                        case 11:
                            Config.TIME_BETWEEN_SWITCH = Math.max((Config.TIME_BETWEEN_SWITCH - 5), 1);
                            updateInventory();
                            break;
                        case 12:
                            Config.TIME_BETWEEN_SWITCH = Math.max((Config.TIME_BETWEEN_SWITCH - 1), 1);
                            updateInventory();
                            break;
                        case 14:
                            Config.TIME_BETWEEN_SWITCH = Math.min((Config.TIME_BETWEEN_SWITCH + 1), 120);
                            updateInventory();
                            break;
                        case 15:
                            Config.TIME_BETWEEN_SWITCH = Math.min((Config.TIME_BETWEEN_SWITCH + 5), 120);
                            updateInventory();
                            break;
                        case 16:
                            Config.TIME_BETWEEN_SWITCH = Math.min((Config.TIME_BETWEEN_SWITCH + 10), 120);
                            updateInventory();
                            break;


                        case 21:
                            Config.PLAYER_AMOUNT_SWITCH = Math.max((Config.PLAYER_AMOUNT_SWITCH - 1), 1);
                            updateInventory();
                            break;
                        case 23:
                            Config.PLAYER_AMOUNT_SWITCH = Math.min((Config.PLAYER_AMOUNT_SWITCH + 1), 10);
                            updateInventory();
                            break;


                        case 28:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.max((Config.TIME_BEFORE_FIRST_SWITCH - 10), 5);
                            updateInventory();
                            break;
                        case 29:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.max((Config.TIME_BEFORE_FIRST_SWITCH - 5), 5);
                            updateInventory();
                            break;
                        case 30:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.max((Config.TIME_BEFORE_FIRST_SWITCH - 1), 5);
                            updateInventory();
                            break;
                        case 32:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.min((Config.TIME_BEFORE_FIRST_SWITCH + 1), 120);
                            updateInventory();
                            break;
                        case 33:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.min((Config.TIME_BEFORE_FIRST_SWITCH + 5), 120);
                            updateInventory();
                            break;
                        case 34:
                            Config.TIME_BEFORE_FIRST_SWITCH = Math.min((Config.TIME_BEFORE_FIRST_SWITCH + 10), 120);
                            updateInventory();
                            break;

                        case 40:
                            Config.INV_SWITCH = !Config.INV_SWITCH;
                            updateInventory();
                            break;

                        case 49:
                            HostMenu.open(playerInfo);
                            break;
                    }

                }
            }
        }

    }

}
