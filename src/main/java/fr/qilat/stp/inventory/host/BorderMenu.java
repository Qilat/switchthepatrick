package fr.qilat.stp.inventory.host;

import fr.qilat.stp.Config;
import fr.qilat.stp.UtilItem;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class BorderMenu {

    private static final String INVENTORY_NAME = ChatColor.GREEN + "Bordures";
    public static Inventory inventory;
    private static UtilItem returnItem = new UtilItem();
    private static UtilItem bordureSizeItem = new UtilItem();
    private static UtilItem borderSizeInc1Item = new UtilItem();
    private static UtilItem borderSizeInc5Item = new UtilItem();
    private static UtilItem borderSizeInc10Item = new UtilItem();
    private static UtilItem borderSizeDec1Item = new UtilItem();
    private static UtilItem borderSizeDec5Item = new UtilItem();
    private static UtilItem borderSizeDec10Item = new UtilItem();

    private static UtilItem borderSpeedItem = new UtilItem();
    private static UtilItem borderSpeedInc1Item = new UtilItem();
    private static UtilItem borderSpeedInc2Item = new UtilItem();
    private static UtilItem borderSpeedInc5Item = new UtilItem();
    private static UtilItem borderSpeedDec1Item = new UtilItem();
    private static UtilItem borderSpeedDec2Item = new UtilItem();
    private static UtilItem borderSpeedDec5Item = new UtilItem();

    private static UtilItem timeBeforeBorderItem = new UtilItem();
    private static UtilItem timeBeforeBorderInc1Item = new UtilItem();
    private static UtilItem timeBeforeBorderInc5Item = new UtilItem();
    private static UtilItem timeBeforeBorderInc10Item = new UtilItem();
    private static UtilItem timeBeforeBorderDec1Item = new UtilItem();
    private static UtilItem timeBeforeBorderDec5Item = new UtilItem();
    private static UtilItem timeBeforeBorderDec10Item = new UtilItem();

    static {
        returnItem.setType(Material.WOOD_DOOR);
        returnItem.setName("Retour");

        bordureSizeItem.setName("Taille : " + Config.MAP_SIZE + " x " + Config.MAP_SIZE);
        bordureSizeItem.setType(Material.BARRIER);
        borderSizeDec1Item.setName("-1");
        borderSizeDec1Item.setType(Material.WOOD_AXE);
        borderSizeDec5Item.setName("-10");
        borderSizeDec5Item.setType(Material.IRON_AXE);
        borderSizeDec10Item.setName("-100");
        borderSizeDec10Item.setType(Material.DIAMOND_AXE);
        borderSizeInc1Item.setName("+1");
        borderSizeInc1Item.setType(Material.WOOD_AXE);
        borderSizeInc5Item.setName("+5");
        borderSizeInc5Item.setType(Material.IRON_AXE);
        borderSizeInc10Item.setName("+10");
        borderSizeInc10Item.setType(Material.DIAMOND_AXE);

        borderSpeedItem.setName(Config.BORDER_SPEED + " blocs par seconde");
        borderSpeedItem.setType(Material.FEATHER);
        borderSpeedDec1Item.setName("-1");
        borderSpeedDec1Item.setType(Material.WOOD_AXE);
        borderSpeedDec2Item.setName("-2");
        borderSpeedDec2Item.setType(Material.IRON_AXE);
        borderSpeedDec5Item.setName("-5");
        borderSpeedDec5Item.setType(Material.DIAMOND_AXE);
        borderSpeedInc1Item.setName("+1");
        borderSpeedInc1Item.setType(Material.WOOD_AXE);
        borderSpeedInc2Item.setName("+2");
        borderSpeedInc2Item.setType(Material.IRON_AXE);
        borderSpeedInc5Item.setName("+5");
        borderSpeedInc5Item.setType(Material.DIAMOND_AXE);

        timeBeforeBorderItem.setName(Config.TIME_BEFORE_BORDER + " minutes avant bordure");
        timeBeforeBorderItem.setType(Material.WATCH);
        timeBeforeBorderDec1Item.setName("-1");
        timeBeforeBorderDec1Item.setType(Material.WOOD_AXE);
        timeBeforeBorderDec5Item.setName("-5");
        timeBeforeBorderDec5Item.setType(Material.IRON_AXE);
        timeBeforeBorderDec10Item.setName("-10");
        timeBeforeBorderDec10Item.setType(Material.DIAMOND_AXE);
        timeBeforeBorderInc1Item.setName("+1");
        timeBeforeBorderInc1Item.setType(Material.WOOD_AXE);
        timeBeforeBorderInc5Item.setName("+5");
        timeBeforeBorderInc5Item.setType(Material.IRON_AXE);
        timeBeforeBorderInc10Item.setName("+10");
        timeBeforeBorderInc10Item.setType(Material.DIAMOND_AXE);
    }

    public static void updateInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, 9 * 6, INVENTORY_NAME);
            inventory.setItem(49, returnItem);

            inventory.setItem(10, borderSizeDec10Item);
            inventory.setItem(11, borderSizeDec5Item);
            inventory.setItem(12, borderSizeDec1Item);
            inventory.setItem(13, bordureSizeItem);
            inventory.setItem(14, borderSizeInc1Item);
            inventory.setItem(15, borderSizeInc5Item);
            inventory.setItem(16, borderSizeInc10Item);

            inventory.setItem(19, borderSpeedDec5Item);
            inventory.setItem(20, borderSpeedDec2Item);
            inventory.setItem(21, borderSpeedDec1Item);
            inventory.setItem(22, borderSpeedItem);
            inventory.setItem(23, borderSpeedInc1Item);
            inventory.setItem(24, borderSpeedInc2Item);
            inventory.setItem(25, borderSpeedInc5Item);

            inventory.setItem(28, timeBeforeBorderDec10Item);
            inventory.setItem(29, timeBeforeBorderDec5Item);
            inventory.setItem(30, timeBeforeBorderDec1Item);
            inventory.setItem(31, timeBeforeBorderItem);
            inventory.setItem(32, timeBeforeBorderInc1Item);
            inventory.setItem(33, timeBeforeBorderInc5Item);
            inventory.setItem(34, timeBeforeBorderInc10Item);
        }

        bordureSizeItem.setName(Config.TEAM_AMOUNT + " équipes");
        borderSpeedItem.setName(Config.BORDER_SPEED + " blocs par seconde");
        timeBeforeBorderItem.setName(Config.TIME_BEFORE_BORDER + " minutes avant bordure");

    }

    public static void open(PlayerInfo playerInfo) {
        updateInventory();
        playerInfo.getBukkitPlayer().openInventory(inventory);
    }

    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onClick(InventoryClickEvent event) {
            if (PhaseManager.currentPhase == PhaseEnum.LOBBY) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getWhoClicked().getUniqueId());
                if (event.getClickedInventory().getName().equals(INVENTORY_NAME)
                        && Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(event.getWhoClicked().getUniqueId())) {
                    event.setCancelled(true);

                    switch (event.getSlot()) {
                        case 10:
                            Config.MAP_SIZE = Math.max((Config.MAP_SIZE - 100), 100);
                            updateInventory();
                            break;
                        case 11:
                            Config.MAP_SIZE = Math.max((Config.MAP_SIZE - 10), 100);
                            updateInventory();
                            break;
                        case 12:
                            Config.MAP_SIZE = Math.max((Config.MAP_SIZE - 1), 100);
                            updateInventory();
                            break;
                        case 14:
                            Config.MAP_SIZE = Math.min((Config.MAP_SIZE + 1), 10000);
                            updateInventory();
                            break;
                        case 15:
                            Config.MAP_SIZE = Math.min((Config.MAP_SIZE + 10), 10000);
                            updateInventory();
                            break;
                        case 16:
                            Config.MAP_SIZE = Math.min((Config.MAP_SIZE + 100), 10000);
                            updateInventory();
                            break;


                        case 19:
                            Config.BORDER_SPEED = Math.max((Config.BORDER_SPEED - 5), 1);
                            updateInventory();
                            break;
                        case 20:
                            Config.BORDER_SPEED = Math.max((Config.BORDER_SPEED - 2), 1);
                            updateInventory();
                            break;
                        case 21:
                            Config.BORDER_SPEED = Math.max((Config.BORDER_SPEED - 1), 1);
                            updateInventory();
                            break;
                        case 23:
                            Config.BORDER_SPEED = Math.min((Config.BORDER_SPEED + 1), 10);
                            updateInventory();
                            break;
                        case 24:
                            Config.BORDER_SPEED = Math.min((Config.BORDER_SPEED + 2), 10);
                            updateInventory();
                            break;
                        case 25:
                            Config.BORDER_SPEED = Math.min((Config.BORDER_SPEED + 5), 10);
                            updateInventory();
                            break;

                        case 28:
                            Config.TIME_BEFORE_BORDER = Math.max((Config.TIME_BEFORE_BORDER - 10), 5);
                            updateInventory();
                            break;
                        case 29:
                            Config.TIME_BEFORE_BORDER = Math.max((Config.TIME_BEFORE_BORDER - 5), 5);
                            updateInventory();
                            break;
                        case 30:
                            Config.TIME_BEFORE_BORDER = Math.max((Config.TIME_BEFORE_BORDER - 1), 5);
                            updateInventory();
                            break;
                        case 32:
                            Config.TIME_BEFORE_BORDER = Math.min((Config.TIME_BEFORE_BORDER + 1), 120);
                            updateInventory();
                            break;
                        case 33:
                            Config.TIME_BEFORE_BORDER = Math.min((Config.TIME_BEFORE_BORDER + 5), 120);
                            updateInventory();
                            break;
                        case 34:
                            Config.TIME_BEFORE_BORDER = Math.min((Config.TIME_BEFORE_BORDER + 10), 120);
                            updateInventory();
                            break;

                        case 49:
                            HostMenu.open(playerInfo);
                            break;
                    }

                }
            }
        }

    }


}
