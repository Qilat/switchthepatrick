package fr.qilat.stp.inventory.host;

import fr.qilat.stp.Config;
import fr.qilat.stp.UtilItem;
import fr.qilat.stp.commands.SaveInventoryCommand;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class HostMenu {

    private static final String INVENTORY_NAME = ChatColor.GREEN + "Configuration générale";
    public static Inventory inventory = null;
    private static UtilItem playerTeamSetupItem = new UtilItem(Material.ARMOR_STAND);
    private static UtilItem beginInvSetupItem = new UtilItem();
    private static UtilItem deathInvSetupItem = new UtilItem();
    private static UtilItem borderSetupItem = new UtilItem();
    private static UtilItem pvpSetupItem = new UtilItem();
    private static UtilItem switchSetupItem = new UtilItem();
    private static UtilItem whitelistSetupItem = new UtilItem();
    private static UtilItem netherSetupItem = new UtilItem();
    private static UtilItem endSetupItem = new UtilItem();
    private static UtilItem chatSetupItem = new UtilItem();
    private static UtilItem fishingRodSetupItem = new UtilItem();
    private static UtilItem potionSetupItem = new UtilItem();
    private static UtilItem armorCraftSetupItem = new UtilItem();

    static {
        playerTeamSetupItem.setType(Material.ARMOR_STAND);
        beginInvSetupItem.setType(Material.SAPLING);
        deathInvSetupItem.setType(Material.DEAD_BUSH);
        borderSetupItem.setType(Material.ACACIA_DOOR);
        pvpSetupItem.setType(Material.DIAMOND_SWORD);
        switchSetupItem.setType(Material.CARROT);
        whitelistSetupItem.setType(Material.BLAZE_ROD);
        netherSetupItem.setType(Material.OBSIDIAN);
        endSetupItem.setType(Material.EYE_OF_ENDER);
        chatSetupItem.setType(Material.ARMOR_STAND);
        fishingRodSetupItem.setType(Material.FISHING_ROD);
        potionSetupItem.setType(Material.POTION);
        armorCraftSetupItem.setType(Material.DIAMOND_CHESTPLATE);
        playerTeamSetupItem.setName("Joueurs/Teams");
        beginInvSetupItem.setName("Inventaire de départ");
        deathInvSetupItem.setName("Inventaire de mort");
        borderSetupItem.setName("Bordures");
        pvpSetupItem.setName("Pvp");
        switchSetupItem.setName("Switch");
        whitelistSetupItem.setName("Whitelist");
        netherSetupItem.setName("Nether");
        endSetupItem.setName("End");
        chatSetupItem.setName("Chat global");
        fishingRodSetupItem.setName("Canne à pêche");
        potionSetupItem.setName("Potions");
        armorCraftSetupItem.setName("Armures");
    }
    // Joueur/Team 1
    // Inv départ 2
    // Inv death 3

    //Bordure 4
    //PVP 5
    //Switch 6

    //whitelist 7
    //nether 8
    //End 9
    //chat global A
    //canne à peche B

    //potion C
    //armorCraft D
    //XP E

    // X X X X X X X X X
    // X 1 2 3 X 4 5 6 X
    // X X C X X X D X X
    // X X X 8 9 A X X X
    // X X 7 X E X B X X
    // X X X X X X X X X

    public static void updateInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, 9 * 6, INVENTORY_NAME);
            inventory.setItem(10, playerTeamSetupItem);
            inventory.setItem(11, beginInvSetupItem);
            inventory.setItem(12, deathInvSetupItem);
            inventory.setItem(14, borderSetupItem);
            inventory.setItem(15, pvpSetupItem);
            inventory.setItem(16, switchSetupItem);
            inventory.setItem(20, potionSetupItem);
            inventory.setItem(24, armorCraftSetupItem);
            inventory.setItem(30, netherSetupItem);
            inventory.setItem(31, endSetupItem);
            inventory.setItem(32, chatSetupItem);
            inventory.setItem(38, whitelistSetupItem);
            inventory.setItem(42, fishingRodSetupItem);
        }

        netherSetupItem.setLore(Config.NETHER_ENABLED ? (ChatColor.GREEN + "Activé") : (ChatColor.RED + "Désactivé"));
        endSetupItem.setLore(Config.END_ENABLED ? (ChatColor.GREEN + "Activé") : (ChatColor.RED + "Désactivé"));
        chatSetupItem.setLore(Config.CHAT_ENABLED ? (ChatColor.GREEN + "Activé") : (ChatColor.RED + "Désactivé"));
        whitelistSetupItem.setLore(Config.WHITELIST_ENABLED ? (ChatColor.GREEN + "Activé") : (ChatColor.RED + "Désactivé"));
        fishingRodSetupItem.setLore(Config.FISHING_ROD_ENABLED ? (ChatColor.GREEN + "Activé") : (ChatColor.RED + "Désactivé"));
    }

    public static void open(PlayerInfo playerInfo) {
        updateInventory();
        playerInfo.getBukkitPlayer().openInventory(inventory);
    }


    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onInteract(InventoryClickEvent event) {
            if (PhaseManager.currentPhase == PhaseEnum.LOBBY) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getWhoClicked().getUniqueId());
                if (event.getClickedInventory().getName().equals(INVENTORY_NAME)
                        && Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(event.getWhoClicked().getUniqueId())) {
                    event.setCancelled(true);
                    switch (event.getSlot()) {
                        case 10:
                            PlayerTeamMenu.open(playerInfo);
                            break;
                        case 11:
                            playerInfo.getBukkitPlayer().getInventory().clear();
                            playerInfo.getBukkitPlayer().setGameMode(GameMode.CREATIVE);
                            playerInfo.getBukkitPlayer().sendMessage(
                                    ChatColor.GREEN + "Vous pouvez désormais définir l'inventaire de départ. " +
                                            "Une fois terminée, faites /save-inventory pour sauvegarder l'inventaire");
                            playerInfo.getBukkitPlayer().closeInventory();
                            SaveInventoryCommand.editingPlayers.put(playerInfo.getBukkitPlayer().getUniqueId(), SaveInventoryCommand.Type.BEGIN);
                            break;
                        case 12:
                            playerInfo.getBukkitPlayer().getInventory().clear();
                            playerInfo.getBukkitPlayer().setGameMode(GameMode.CREATIVE);
                            playerInfo.getBukkitPlayer().sendMessage(
                                    ChatColor.GREEN + "Vous pouvez désormais définir l'inventaire de mort. " +
                                            "Une fois terminée, faites /save-inventory pour sauvegarder l'inventaire");
                            playerInfo.getBukkitPlayer().closeInventory();
                            SaveInventoryCommand.editingPlayers.put(playerInfo.getBukkitPlayer().getUniqueId(), SaveInventoryCommand.Type.DEATH);
                            break;
                        case 14:
                            BorderMenu.open(playerInfo);
                            break;
                        case 15:
                            PVPMenu.open(playerInfo);
                            break;
                        case 16:
                            SwitchMenu.open(playerInfo);
                            break;
                        case 20:
                            break;
                        case 24:
                            break;
                        case 30:
                            Config.NETHER_ENABLED = !Config.NETHER_ENABLED;
                            updateInventory();
                            break;
                        case 31:
                            Config.END_ENABLED = !Config.END_ENABLED;
                            updateInventory();
                            break;
                        case 32:
                            Config.CHAT_ENABLED = !Config.CHAT_ENABLED;
                            updateInventory();
                            break;
                        case 38:
                            Config.WHITELIST_ENABLED = !Config.WHITELIST_ENABLED;
                            Bukkit.setWhitelist(Config.WHITELIST_ENABLED);
                            updateInventory();
                            break;
                        case 42:
                            Config.FISHING_ROD_ENABLED = !Config.FISHING_ROD_ENABLED;
                            updateInventory();
                            break;
                    }

                }
            }
        }

    }

}
