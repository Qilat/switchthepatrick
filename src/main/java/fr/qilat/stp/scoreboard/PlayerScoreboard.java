package fr.qilat.stp.scoreboard;

import fr.qilat.stp.playerinfo.PlayerInfo;
import lombok.Getter;
import lombok.Setter;

public class PlayerScoreboard {
    @Getter
    private CustomScoreboard scoreboard;
    @Getter
    private PlayerInfo playerInfo;

    private String[] lines = new String[15];
    @Getter
    @Setter
    private boolean hide = false;

    public PlayerScoreboard(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
        this.scoreboard = new CustomScoreboard("§eErisoum");
        if (this.playerInfo.getBukkitPlayer() != null) {
            this.scoreboard.destroy(this.playerInfo.getBukkitPlayer());
            this.scoreboard.create(this.playerInfo.getBukkitPlayer());
        } else {
            return;
        }
        init();
        update();
    }

    public void init() {
        this.lines[6] = "§eDurée: 0:00";
        this.lines[5] = "§eSwitch: 40:00";
        this.lines[4] = "§2";
        this.lines[3] = "§eJoueurs: 0";
        this.lines[2] = "§eEquipes: 0";
        this.lines[1] = "§f";
        this.lines[0] = "§aSwitchThePatrick";

        sendScoreboard();
    }

    public void update() {
        this.lines[7] = "§eDurée: ";
        this.lines[6] = "§eSwitch: ";
        this.lines[5] = "§2";
        this.lines[4] = "§eJoueurs: ";
        this.lines[3] = "§eEquipes: ";
        this.lines[2] = "§f";
        this.lines[1] = "§3";
        this.lines[0] = "SwitchThePatrick";

        sendScoreboard();
    }


    public void sendScoreboard() {
        if (!this.hide) {
            for (int i = 0; i < this.lines.length; i++)
                if (this.lines[i] != null)
                    this.scoreboard.setLine(i + 1, this.lines[i]);
        }
    }

    public void setLine(int id, String string) {
        this.lines[id] = string;
    }
}
