package fr.qilat.stp.team;

import fr.qilat.stp.Config;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;

public class TeamManager {

    private static final ChatColor[] CHAT_COLORS = {
            ChatColor.BLACK, ChatColor.DARK_BLUE, ChatColor.DARK_GREEN,
            ChatColor.DARK_AQUA, ChatColor.DARK_RED, ChatColor.DARK_PURPLE,
            ChatColor.GOLD, ChatColor.GRAY, ChatColor.DARK_GRAY,
            ChatColor.BLUE, ChatColor.GREEN, ChatColor.AQUA,
            ChatColor.RED, ChatColor.LIGHT_PURPLE, ChatColor.YELLOW,
            ChatColor.WHITE
    };
    private static final String[] PREFIX = {"", "★", "☀", "Ø", "❤", "¤", "~"};

    private static ArrayList<Team> teams = new ArrayList<>(Arrays.asList(new Team(CHAT_COLORS[0], PREFIX[0]), new Team(CHAT_COLORS[1], PREFIX[0])));

    /**
     * Add new team
     */
    public static void addTeam() {
        Team t = new Team();
        t.setColor(CHAT_COLORS[teams.size() % CHAT_COLORS.length]);
        t.setPrefix(PREFIX[Math.floorDiv(teams.size(), CHAT_COLORS.length)]);
        teams.add(t);
    }

    /**
     * Remove last team
     */
    public static void removeLastTeam() {
        teams.remove(teams.size() - 1);
    }

    public static void teamBalancer() {
        Team[] unfilledTeams = (Team[]) teams.stream().filter(team -> team.getPlayerInfos().size() < Config.MAX_PLAYERS_PER_TEAM).toArray();

    }

}
