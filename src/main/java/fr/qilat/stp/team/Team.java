package fr.qilat.stp.team;

import fr.qilat.stp.playerinfo.PlayerInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.ChatColor;

import java.util.ArrayList;

@NoArgsConstructor
public class Team {


    @Getter
    @Setter
    private ChatColor color;
    @Getter
    @Setter
    private String prefix;

    @Getter
    private ArrayList<PlayerInfo> playerInfos = new ArrayList<>();

    public Team(ChatColor color, String prefix) {
        this.color = color;
        this.prefix = prefix;
    }
}
