package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.PortalCreateEvent;

public class PortalListener implements Listener {

    @EventHandler
    public void onPortal(PortalCreateEvent event) {
        if (event.getReason().equals(PortalCreateEvent.CreateReason.FIRE)
                && !Config.NETHER_ENABLED) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPortal(PlayerInteractEvent event) {
        if (event.getItem() != null
                && event.getItem().getType().equals(Material.EYE_OF_ENDER)
                && event.getClickedBlock() != null
                && event.getClickedBlock().getType().equals(Material.ENDER_PORTAL_FRAME)
                && !Config.END_ENABLED) {
            event.setCancelled(true);
        }
    }

}
