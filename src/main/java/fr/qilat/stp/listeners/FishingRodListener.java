package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class FishingRodListener implements Listener {

    @EventHandler
    public void onFishRodCraft(CraftItemEvent event) {
        if (!Config.FISHING_ROD_ENABLED) {
            if (event.getRecipe().getResult().getType().equals(Material.FISHING_ROD)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onFishRodCraft(PlayerInteractEvent event) {
        if (!Config.FISHING_ROD_ENABLED) {
            if (event.getItem().getType().equals(Material.FISHING_ROD)) {
                event.setCancelled(true);
            }
        }
    }

}
