package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class XPListener implements Listener {

    @EventHandler
    public void onXPDrop(PlayerExpChangeEvent event) {
        event.setAmount(event.getAmount() * Config.XP_RATE);
    }

}
