package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class FightListener implements Listener {

    @EventHandler
    public void onFight(EntityDamageByEntityEvent event) {

        if (PhaseManager.currentPhase == PhaseEnum.LOBBY || PhaseManager.currentPhase == PhaseEnum.FINAL) {
            event.setCancelled(true);
        } else if (PhaseManager.currentPhase == PhaseEnum.GAME) {
            if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
                if (!Config.PVP_ACTIVATED) {
                    event.setCancelled(true);
                    return;
                }

                if (!Config.FRIENDLY_FIRE) {
                    PlayerInfo targetPlayerInfo = PlayerManager.getPlayerInfos().get(event.getEntity().getUniqueId());
                    PlayerInfo damagerPlayerInfo = PlayerManager.getPlayerInfos().get(event.getDamager().getUniqueId());
                    if (targetPlayerInfo.getTeam().equals(damagerPlayerInfo.getTeam())) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

}
