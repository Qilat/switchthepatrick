package fr.qilat.stp.listeners;

import fr.qilat.stp.Utils;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getEntity().getUniqueId());
        playerInfo.setAlive(false);
        playerInfo.setDeathInventory(Utils.fromInventoryToMap(event.getEntity()));
        playerInfo.setDeathLoc(event.getEntity().getLocation().clone());

        event.getEntity().setGameMode(GameMode.SPECTATOR);
    }

}
