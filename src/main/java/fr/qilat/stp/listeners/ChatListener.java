package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    public static String formatMessage(PlayerInfo playerInfo, String message) {
        return ChatColor.ITALIC.toString() + ChatColor.GRAY.toString() + " [%scope%] " + playerInfo.getTeam().getColor().toString() + playerInfo.getTeam().getPrefix() + " " + playerInfo.getBukkitPlayer().getDisplayName() + " > " + message;
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (PhaseManager.currentPhase.equals(PhaseEnum.GAME)) {
            event.setCancelled(true);
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getPlayer().getUniqueId());
            if (event.getMessage().startsWith("!")) {
                if (Config.CHAT_ENABLED) {
                    String formattedMsg = formatMessage(playerInfo, event.getMessage()).replace("%scope%", "Général");
                    Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(formattedMsg));
                } else {
                    event.getPlayer().sendMessage("§cLe chat général n'est pas activé.");
                }
            } else {
                String formattedMsg = formatMessage(playerInfo, event.getMessage()).replace("%scope%", "Equipe");
                playerInfo.getTeam().getPlayerInfos().forEach(targetPlayerInfo -> targetPlayerInfo.getBukkitPlayer().sendMessage(formattedMsg));
            }
        } else if (PhaseManager.currentPhase.equals(PhaseEnum.LOBBY)) {
            event.setCancelled(true);
            PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getPlayer().getUniqueId());
            boolean host = Config.HOST != null
                    && Config.HOST.getBukkitPlayer() != null
                    && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId());
            String msg = (host ? (ChatColor.BOLD.toString() + ChatColor.GRAY.toString() + "[HOST] ") : "") + event.getPlayer().getDisplayName() + ChatColor.WHITE + " > " + event.getMessage();
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(msg));
        }
    }


}
