package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import fr.qilat.stp.inventory.host.HostMenu;
import fr.qilat.stp.phase.LobbyPhase;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class OpenBaseMenuListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (PhaseManager.currentPhase == PhaseEnum.LOBBY
                && (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            if (LobbyPhase.netherStar.isSimilar(event.getItem())) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getPlayer().getUniqueId());
                LobbyPhase.openPlayerMenu(playerInfo);
            } else if (LobbyPhase.hostItem.isSimilar(event.getItem())) {
                PlayerInfo playerInfo = PlayerManager.getPlayerInfos().get(event.getPlayer().getUniqueId());
                if (Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(playerInfo.getBukkitPlayer().getUniqueId())) {
                    HostMenu.open(playerInfo);
                }
            }
        }

    }


}
