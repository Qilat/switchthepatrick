package fr.qilat.stp.listeners;

import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BuildListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (PhaseManager.currentPhase == PhaseEnum.LOBBY || PhaseManager.currentPhase == PhaseEnum.FINAL) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(BlockPlaceEvent event) {
        if (!event.getPlayer().isOp() && (PhaseManager.currentPhase == PhaseEnum.LOBBY || PhaseManager.currentPhase == PhaseEnum.FINAL)) {
            event.setCancelled(true);
        }
    }

}
