package fr.qilat.stp.listeners;

import fr.qilat.stp.Config;
import fr.qilat.stp.phase.LobbyPhase;
import fr.qilat.stp.phase.PhaseEnum;
import fr.qilat.stp.phase.PhaseManager;
import fr.qilat.stp.playerinfo.PlayerInfo;
import fr.qilat.stp.playerinfo.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

public class ConnectionListener implements Listener {

    @EventHandler
    public void onConnect(PlayerLoginEvent event) {
        if (PhaseManager.currentPhase != PhaseEnum.LOBBY) {
            if (!PlayerManager.getPlayerInfos().containsKey(event.getPlayer().getUniqueId())) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "La partie a déjà commencée.");
            }
        } else if (Bukkit.getOnlinePlayers().size() >= (Config.TEAM_AMOUNT * Config.MAX_PLAYERS_PER_TEAM)) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "La partie est déjà pleine.");
        }
    }

    @EventHandler
    public void onJoinListener(PlayerJoinEvent event) {
        PlayerInfo playerInfo = PlayerManager.addPlayerInfo(event.getPlayer());
        PlayerManager.getPlayerInfos().values().forEach(targetpInfo -> {
            if (targetpInfo.isInvisible()) {
                event.getPlayer().hidePlayer(targetpInfo.getBukkitPlayer());
            }
        });

        switch (PhaseManager.currentPhase) {
            case LOBBY:
                playerInfo.getBukkitPlayer().setGameMode(GameMode.SURVIVAL);
                playerInfo.getBukkitPlayer().getInventory().clear();
                if (Config.HOST != null
                        && Config.HOST.getBukkitPlayer() != null
                        && Config.HOST.getBukkitPlayer().getUniqueId().equals(event.getPlayer().getUniqueId())) {
                    LobbyPhase.setHostInventory(playerInfo);
                } else {
                    LobbyPhase.setPlayerInventory(playerInfo);
                }
                break;
            case GAME:
                break;
            case FINAL:
                break;
        }

    }

}
