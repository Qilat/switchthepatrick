package fr.qilat.stp;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Utils {

    public static HashMap<Integer, ItemStack> fromInventoryToMap(Player player) {
        HashMap<Integer, ItemStack> map = new HashMap<>();
        for (int i = 0; i < 45; i++) {
            map.put(i, player.getInventory().getItem(i));
        }
        return map;
    }
}
